USE [master]
GO
/****** Object:  Database [MoviesDb]    Script Date: 9/5/2020 2:29:48 PM ******/
CREATE DATABASE [MoviesDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MoviesDb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\MoviesDb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MoviesDb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\MoviesDb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [MoviesDb] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MoviesDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MoviesDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MoviesDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MoviesDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MoviesDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MoviesDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [MoviesDb] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [MoviesDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MoviesDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MoviesDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MoviesDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MoviesDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MoviesDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MoviesDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MoviesDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MoviesDb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [MoviesDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MoviesDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MoviesDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MoviesDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MoviesDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MoviesDb] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [MoviesDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MoviesDb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MoviesDb] SET  MULTI_USER 
GO
ALTER DATABASE [MoviesDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MoviesDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MoviesDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MoviesDb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MoviesDb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MoviesDb] SET QUERY_STORE = OFF
GO
USE [MoviesDb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 9/5/2020 2:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Actor]    Script Date: 9/5/2020 2:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[MiddleName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[Gender] [nvarchar](max) NULL,
	[DOB] [datetime2](7) NOT NULL,
	[Biography] [nvarchar](max) NULL,
	[Picture] [nvarchar](max) NULL,
 CONSTRAINT [PK_Actor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Character]    Script Date: 9/5/2020 2:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Character](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[Gender] [nvarchar](max) NULL,
	[Picture] [nvarchar](max) NULL,
 CONSTRAINT [PK_Character] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Director]    Script Date: 9/5/2020 2:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Director](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](max) NULL,
 CONSTRAINT [PK_Director] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Franchise]    Script Date: 9/5/2020 2:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Franchise](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Franchise] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movie]    Script Date: 9/5/2020 2:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movie](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MovieTitle] [nvarchar](max) NULL,
	[Genre] [nvarchar](max) NULL,
	[ReleaseYear] [nvarchar](max) NULL,
	[DirectorID] [int] NOT NULL,
	[Picture] [nvarchar](max) NULL,
	[Trailer] [nvarchar](max) NULL,
	[FranchiseID] [int] NOT NULL,
 CONSTRAINT [PK_Movie] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MovieCharacter]    Script Date: 9/5/2020 2:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovieCharacter](
	[CharacterID] [int] NOT NULL,
	[MovieID] [int] NOT NULL,
	[ActorID] [int] NULL,
 CONSTRAINT [PK_MovieCharacter] PRIMARY KEY CLUSTERED 
(
	[CharacterID] ASC,
	[MovieID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200826135653_InitialMigration', N'3.1.7')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200828125025_upadtecharacter', N'3.1.7')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200828132416_updateTables', N'3.1.7')
GO
SET IDENTITY_INSERT [dbo].[Actor] ON 

INSERT [dbo].[Actor] ([ID], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [Biography], [Picture]) VALUES (2, N'Robert', NULL, N'Downey', N'male', CAST(N'1965-04-04T00:00:00.0000000' AS DateTime2), N'Cool dude ', N'https://www.imdb.com/name/nm0000375/mediaviewer/rm421447168')
INSERT [dbo].[Actor] ([ID], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [Biography], [Picture]) VALUES (3, N'Chris', NULL, N'Evans', N'male', CAST(N'1981-06-13T00:00:00.0000000' AS DateTime2), N'another cool buff dude', N'https://www.imdb.com/name/nm0262635/mediaviewer/rm1966443008')
INSERT [dbo].[Actor] ([ID], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [Biography], [Picture]) VALUES (4, N'Chris', NULL, N'Hemsworth', N'male', CAST(N'1983-08-11T00:00:00.0000000' AS DateTime2), N'really buff dude', N'https://www.imdb.com/name/nm1165110/mediaviewer/rm3936529408')
INSERT [dbo].[Actor] ([ID], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [Biography], [Picture]) VALUES (5, N'Mark', NULL, N'Ruffalo', N'male', CAST(N'1967-11-22T00:00:00.0000000' AS DateTime2), N'calm dude but scary when angry', N'https://www.imdb.com/name/nm0749263/mediaviewer/rm1520514560')
INSERT [dbo].[Actor] ([ID], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [Biography], [Picture]) VALUES (6, N'Ben', NULL, N'Affleck', N'male', CAST(N'1972-08-15T00:00:00.0000000' AS DateTime2), N'played alot of movies', N'https://www.imdb.com/name/nm0000255/mediaviewer/rm647596800')
INSERT [dbo].[Actor] ([ID], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [Biography], [Picture]) VALUES (7, N'Henry', NULL, N'Cavill', N'male', CAST(N'1983-05-05T00:00:00.0000000' AS DateTime2), N'super buff dude aswell', N'https://www.imdb.com/name/nm0147147/mediaviewer/rm1318763008')
INSERT [dbo].[Actor] ([ID], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [Biography], [Picture]) VALUES (9, N'Gal', NULL, N'Gadot', N'female', CAST(N'1985-04-30T00:00:00.0000000' AS DateTime2), N'badass woman that went to the israeli army', N'https://www.imdb.com/name/nm2933757/mediaviewer/rm2906398720')
INSERT [dbo].[Actor] ([ID], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [Biography], [Picture]) VALUES (10, N'Edward', NULL, N'Norton', N'male', CAST(N'1969-08-18T00:00:00.0000000' AS DateTime2), N'good hulk', N'https://www.imdb.com/name/nm0001570/mediaviewer/rm3529540352?ref_=nm_ov_ph')
SET IDENTITY_INSERT [dbo].[Actor] OFF
GO
SET IDENTITY_INSERT [dbo].[Character] ON 

INSERT [dbo].[Character] ([ID], [FullName], [Alias], [Gender], [Picture]) VALUES (2, N'Superman', N'Ken', N'Male', N'https://www.google.com/search?q=superman&tbm=isch&ved=2ahUKEwi6m8Lm8tHrAhX4wsQBHYZ-BR0Q2-cCegQIABAA&oq=superman&gs_lcp=CgNpbWcQA1AAWABg4PIIaABwAHgAgAEAiAEAkgEAmAEAqgELZ3dzLXdpei1pbWc&sclient=img&ei=hnRTX7rAO_iFk74Phv2V6AE&bih=722&biw=1536')
INSERT [dbo].[Character] ([ID], [FullName], [Alias], [Gender], [Picture]) VALUES (3, N'Iron Man', N'Tony Stark', N'Male', N'https://www.google.com/search?q=iron+man&tbm=isch&hl=en&sa=X&ved=2ahUKEwiI86K989HrAhWKvCoKHQFPDgQQBXoECAEQUw&biw=1519&bih=722')
INSERT [dbo].[Character] ([ID], [FullName], [Alias], [Gender], [Picture]) VALUES (1007, N'The Hulk', NULL, N'Male', N'https://www.google.com/search?q=hulk&tbm=isch&ved=2ahUKEwicwr2-89HrAhVNwioKHeEzCogQ2-cCegQIABAA&oq=hulk&gs_lcp=CgNpbWcQAzIECAAQQzICCAAyBAgAEEMyBAgAEEMyBAgAEEMyAggAMgQIABBDMgQIABBDMgIIADIECAAQQzoHCAAQsQMQQzoFCAAQsQNQ_Ik9WJaOPWCSkT1oAHAAeACAAW-IAdgCkgEDMy4xmAEAoAEBqgELZ3dzLXdpei1pbWfAAQE&sclient=img&ei=P3VTX9ymG82EqwHh56jACA&bih=722&biw=1519&hl=en')
INSERT [dbo].[Character] ([ID], [FullName], [Alias], [Gender], [Picture]) VALUES (1008, N'Wonder Woman', N'badass', N'Female', N'https://www.google.com/search?q=wonder+woman&tbm=isch&ved=2ahUKEwjUvP2c99HrAhXXwCoKHYq-A2EQ2-cCegQIABAA&oq=wonder+woman&gs_lcp=CgNpbWcQAzIECAAQQzICCAAyBAgAEEMyBAgAEEMyAggAMgIIADIECAAQQzICCAAyBAgAEEMyBAgAEEM6BAgjECc6BwgAELEDEEM6BQgAELEDUNv8AViKhgJgwYcCaABwAHgAgAFUiAHpBpIBAjEymAEAoAEBqgELZ3dzLXdpei1pbWfAAQE&sclient=img&ei=KnlTX9T_ONeBqwGK_Y6IBg&bih=722&biw=1519&hl=en')
INSERT [dbo].[Character] ([ID], [FullName], [Alias], [Gender], [Picture]) VALUES (1009, N'Batman', N'Dark knight', N'male', N'https://www.google.com/search?q=batman&tbm=isch&ved=2ahUKEwjevPqt99HrAhVLuyoKHd-HA9YQ2-cCegQIABAA&oq=batman&gs_lcp=CgNpbWcQAzIECAAQQzIECAAQQzIECAAQQzIECAAQQzIECAAQQzIECAAQQzIECAAQQzIECAAQQzIECAAQQzIECAAQQzoECCMQJzoHCAAQsQMQQzoCCABQpZYBWMKaAWC3mwFoAHAAeACAAVuIAeADkgEBNpgBAKABAaoBC2d3cy13aXotaW1nwAEB&sclient=img&ei=TnlTX97dIMv2qgHfj46wDQ&bih=722&biw=1519&hl=en')
INSERT [dbo].[Character] ([ID], [FullName], [Alias], [Gender], [Picture]) VALUES (1010, N'Superman', N'Ken Klark', N'male', N'https://www.google.com/search?q=superman&tbm=isch&ved=2ahUKEwi3_pe499HrAhXXwCoKHYq-A2EQ2-cCegQIABAA&oq=superman&gs_lcp=CgNpbWcQAzIECCMQJzIECAAQQzIECAAQQzIECAAQQzIECAAQQzICCAAyAggAMgIIADICCAAyBAgAEEM6BwgAELEDEENQ3NUBWKTbAWDx3AFoAHAAeACAAVSIAcYEkgEBOJgBAKABAaoBC2d3cy13aXotaW1nwAEB&sclient=img&ei=Y3lTX_fAPNeBqwGK_Y6IBg&bih=722&biw=1519&hl=en')
INSERT [dbo].[Character] ([ID], [FullName], [Alias], [Gender], [Picture]) VALUES (1011, N'Thor', N'God', N'male', N'https://www.google.com/search?q=thor&tbm=isch&ved=2ahUKEwj_i6TG99HrAhUUyCoKHZE6CmsQ2-cCegQIABAA&oq=thor&gs_lcp=CgNpbWcQAzIECAAQQzIECAAQQzIECAAQQzIECAAQQzIECAAQQzIECAAQQzIECAAQQzICCAAyAggAMgQIABBDOgQIIxAnOgcIABCxAxBDUN3bAVjn3gFg498BaABwAHgAgAF3iAHnApIBAzMuMZgBAKABAaoBC2d3cy13aXotaW1nwAEB&sclient=img&ei=gXlTX7_HIZSQqwGR9ajYBg&bih=722&biw=1519&hl=en')
INSERT [dbo].[Character] ([ID], [FullName], [Alias], [Gender], [Picture]) VALUES (1012, N'Captain America', N'Cap', N'male', N'https://www.google.com/search?q=captai+america&tbm=isch&ved=2ahUKEwiB3MTU99HrAhXNwioKHcN8BbcQ2-cCegQIABAA&oq=captai+&gs_lcp=CgNpbWcQARgAMgIIADICCAAyAggAMgIIADICCAAyAggAMgIIADICCAAyAggAMgIIADoECCMQJzoECAAQQzoHCAAQsQMQQzoFCAAQsQNQ1O8DWPb1A2CvgARoAHAAeACAAcMBiAHbBJIBAzYuMZgBAKABAaoBC2d3cy13aXotaW1nwAEB&sclient=img&ei=n3lTX8GQG82FqwHD-ZW4Cw&bih=722&biw=1519&hl=en')
SET IDENTITY_INSERT [dbo].[Character] OFF
GO
SET IDENTITY_INSERT [dbo].[Director] ON 

INSERT [dbo].[Director] ([ID], [FullName]) VALUES (1, N'Sven')
INSERT [dbo].[Director] ([ID], [FullName]) VALUES (2, N'Mr Director')
INSERT [dbo].[Director] ([ID], [FullName]) VALUES (3, N'mr Awsome')
INSERT [dbo].[Director] ([ID], [FullName]) VALUES (4, N'mr Postman')
INSERT [dbo].[Director] ([ID], [FullName]) VALUES (5, N'James')
SET IDENTITY_INSERT [dbo].[Director] OFF
GO
SET IDENTITY_INSERT [dbo].[Franchise] ON 

INSERT [dbo].[Franchise] ([ID], [Name], [Description]) VALUES (1, N'Marvel', N'Marvel universe for fictional superheroes')
INSERT [dbo].[Franchise] ([ID], [Name], [Description]) VALUES (2, N'DC', N'DC universe with various super heroes')
SET IDENTITY_INSERT [dbo].[Franchise] OFF
GO
SET IDENTITY_INSERT [dbo].[Movie] ON 

INSERT [dbo].[Movie] ([ID], [MovieTitle], [Genre], [ReleaseYear], [DirectorID], [Picture], [Trailer], [FranchiseID]) VALUES (2, N'Avengers Infinity war', N'Action, Comedy', N'2019', 1, N'https://www.imdb.com/title/tt4154796/mediaviewer/rm2775147008', N'https://www.imdb.com/video/vi2163260441?playlistId=tt4154796&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movie] ([ID], [MovieTitle], [Genre], [ReleaseYear], [DirectorID], [Picture], [Trailer], [FranchiseID]) VALUES (3, N'Iron Man ', N'Action, Comedy', N'2008', 2, N'https://www.imdb.com/title/tt0371746/mediaviewer/rm1544850432', N'https://www.imdb.com/video/vi447873305?playlistId=tt0371746&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movie] ([ID], [MovieTitle], [Genre], [ReleaseYear], [DirectorID], [Picture], [Trailer], [FranchiseID]) VALUES (4, N'The incredible Hulk', N'Action', N'2008', 3, N'https://www.imdb.com/title/tt0800080/mediaviewer/rm2081134080', N'https://www.imdb.com/video/vi1726152985?playlistId=tt0800080&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movie] ([ID], [MovieTitle], [Genre], [ReleaseYear], [DirectorID], [Picture], [Trailer], [FranchiseID]) VALUES (5, N'Thor Ragnarok', N'Action, Comedy', N'2017', 4, N'https://www.imdb.com/title/tt3501632/mediaviewer/rm1413491712', N'https://www.imdb.com/video/vi4010391833?playlistId=tt3501632&ref_=tt_ov_vi', 1)
INSERT [dbo].[Movie] ([ID], [MovieTitle], [Genre], [ReleaseYear], [DirectorID], [Picture], [Trailer], [FranchiseID]) VALUES (1004, N'Justice League', N'Action', N'2017', 3, N'https://www.imdb.com/title/tt0974015/mediaviewer/rm1061640448', N'https://www.imdb.com/video/vi2454436121?playlistId=tt0974015&ref_=tt_ov_vi', 2)
INSERT [dbo].[Movie] ([ID], [MovieTitle], [Genre], [ReleaseYear], [DirectorID], [Picture], [Trailer], [FranchiseID]) VALUES (1008, N'Batman vs. Superman', N'Action', N'2016', 3, N'https://www.imdb.com/title/tt2975590/mediaviewer/rm2302675456', N'https://www.imdb.com/video/vi1946858521?playlistId=tt2975590&ref_=tt_pr_ov_vi', 2)
INSERT [dbo].[Movie] ([ID], [MovieTitle], [Genre], [ReleaseYear], [DirectorID], [Picture], [Trailer], [FranchiseID]) VALUES (1009, N'Wonder Woman', N'Action, Comedy', N'2017', 2, N'https://www.imdb.com/title/tt0451279/mediaviewer/rm1404907776', N'https://www.imdb.com/video/vi1553381657?playlistId=tt0451279&ref_=tt_ov_vi', 2)
SET IDENTITY_INSERT [dbo].[Movie] OFF
GO
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (3, 2, 2)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (3, 3, 2)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1012, 2, 3)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1011, 2, 4)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1011, 5, 4)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1007, 2, 5)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1007, 5, 5)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1009, 1004, 6)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1009, 1008, 6)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (2, 1004, 7)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (2, 1008, 7)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1010, 1004, 7)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1010, 1008, 7)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1008, 1004, 9)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1008, 1008, 9)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1008, 1009, 9)
INSERT [dbo].[MovieCharacter] ([CharacterID], [MovieID], [ActorID]) VALUES (1007, 4, 10)
GO
/****** Object:  Index [IX_Movie_DirectorID]    Script Date: 9/5/2020 2:29:49 PM ******/
CREATE NONCLUSTERED INDEX [IX_Movie_DirectorID] ON [dbo].[Movie]
(
	[DirectorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Movie_FranchiseID]    Script Date: 9/5/2020 2:29:49 PM ******/
CREATE NONCLUSTERED INDEX [IX_Movie_FranchiseID] ON [dbo].[Movie]
(
	[FranchiseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_MovieCharacter_ActorID]    Script Date: 9/5/2020 2:29:49 PM ******/
CREATE NONCLUSTERED INDEX [IX_MovieCharacter_ActorID] ON [dbo].[MovieCharacter]
(
	[ActorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_MovieCharacter_MovieID]    Script Date: 9/5/2020 2:29:49 PM ******/
CREATE NONCLUSTERED INDEX [IX_MovieCharacter_MovieID] ON [dbo].[MovieCharacter]
(
	[MovieID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Movie]  WITH CHECK ADD  CONSTRAINT [FK_Movie_Director_DirectorID] FOREIGN KEY([DirectorID])
REFERENCES [dbo].[Director] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Movie] CHECK CONSTRAINT [FK_Movie_Director_DirectorID]
GO
ALTER TABLE [dbo].[Movie]  WITH CHECK ADD  CONSTRAINT [FK_Movie_Franchise_FranchiseID] FOREIGN KEY([FranchiseID])
REFERENCES [dbo].[Franchise] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Movie] CHECK CONSTRAINT [FK_Movie_Franchise_FranchiseID]
GO
ALTER TABLE [dbo].[MovieCharacter]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacter_Actor_ActorID] FOREIGN KEY([ActorID])
REFERENCES [dbo].[Actor] ([ID])
GO
ALTER TABLE [dbo].[MovieCharacter] CHECK CONSTRAINT [FK_MovieCharacter_Actor_ActorID]
GO
ALTER TABLE [dbo].[MovieCharacter]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacter_Character_CharacterID] FOREIGN KEY([CharacterID])
REFERENCES [dbo].[Character] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacter] CHECK CONSTRAINT [FK_MovieCharacter_Character_CharacterID]
GO
ALTER TABLE [dbo].[MovieCharacter]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacter_Movie_MovieID] FOREIGN KEY([MovieID])
REFERENCES [dbo].[Movie] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacter] CHECK CONSTRAINT [FK_MovieCharacter_Movie_MovieID]
GO
USE [master]
GO
ALTER DATABASE [MoviesDb] SET  READ_WRITE 
GO
