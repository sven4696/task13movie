﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13.Models
{
    public class MovieCharacter
    {
        public MovieCharacter() { }
        // foreign key
        public int CharacterID { get; set; }

        // Navigation property
        public Character Character { get; set; }

        // foreign key
        public int MovieID { get; set; }

        // Navigation property
        public Movie Movie { get; set; }

        // Navigation property
        public Actor Actor { get; set; }
    }
}
