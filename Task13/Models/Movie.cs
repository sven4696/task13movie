﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13.Models
{
    public class Movie
    {

        public Movie() { }
        public Movie(string movieTitle, string genre, string releaseYear, int directorID, string picture, string trailer, int franchiseID)   {
            MovieTitle = movieTitle;
            Genre = genre;
            ReleaseYear = releaseYear;
            DirectorID = directorID;
            Picture = picture;
            Trailer = trailer;
            FranchiseID = franchiseID;
        }

        public int ID { get; set; }
        public string MovieTitle { get; set; }
        public string Genre { get; set; }

        public string ReleaseYear { get; set; }

        // foreign key
        public int DirectorID { get; set; }

        // Navigation property
        public Director Director { get; set; }

        public string Picture { get; set; }

        public string Trailer { get; set; }

        // Collection navigation property
        public ICollection<MovieCharacter> MovieCharacters { get; set; }

        // Foreign key property
        public int FranchiseID { get; set; }

        // Navigation property
        public Franchise Franchise { get; set; }
    }
}
