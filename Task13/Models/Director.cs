﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13.Models
{
    public class Director
    {
        public Director()
        {

        }
        public Director(string fullName)
        {
            FullName = fullName;
        }

        public int ID { get; set; }

        public string FullName { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
