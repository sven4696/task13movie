﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13.Models
{
    public class Franchise
    {
        public Franchise() { }
        public Franchise(int iD, string name, string description)
        {
            ID = iD;
            Name = name;
            Description = description;
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        // collection navigation property
        public ICollection<Movie> Movies { get; set; }
    }
}
