﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13.Models
{
    public class Actor
    {
        public Actor() { }
        public Actor(int iD, string firstName, string middleName, string lastName, string gender, DateTime dOB, string biography, string picture)
        {
            ID = iD;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            Gender = gender;
            DOB = dOB;
            Biography = biography;
            Picture = picture;
        }

        public int ID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public DateTime DOB { get; set; }

        public string Biography { get; set; }
        public string Picture { get; set; }

        // Foreign key
        // might not need this one
        //public int MovieCharacterID { get; set; }

        // Collection navigation property
        public ICollection<MovieCharacter> MovieCharacters { get; set; }


    }
}
