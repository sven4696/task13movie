﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13.Models
{
    public class Character
    {
        public Character() { }
        public Character(string fullName, string alias, string gender, string picture)
        {
            FullName = fullName;
            Alias = alias;
            Gender = gender;
            Picture = picture;
        }

        public int ID { get; set; }
        public string FullName { get; set; }

        public string Alias { get; set; }

        public string Gender { get; set; }

        public string Picture { get; set; }

        // Foreign key
        // might not need this one
        //public int MovieCharacterID { get; set; }

        // Collection navigation property
        public ICollection<MovieCharacter> MovieCharacters { get; set; }
    }
}
