﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13;
using Task13.DTOs.Franchise;
using Task13.Models;

namespace Task13.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper; 

        public FranchisesController(MovieDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Franchises
        /// <summary>
        /// Get all Franchises from the database
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Franchise>>> GetFranchise()
        {
            return await _context.Franchise.ToListAsync();
        }

        // GET: api/Franchises/5
        /// <summary>
        /// Get a specific Franchise according to its id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDto>> GetFranchise(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            FranchiseDto dto = _mapper.Map<FranchiseDto>(franchise);

            return dto;
        }

        // GET: api/Franchises/5
        /// <summary>
        /// Get all movies from a specific franchise
        /// </summary>
        [HttpGet("moviesf/{id}")]
        public async Task<ActionResult<List<FranchiseAllMoviesDto>>> GetFranchiseMovies(int id)
        {
            var franchise = await _context.Franchise.Include(f => f.Movies).Where(f => f.ID == id).ToListAsync();

            if (franchise == null)
            {
                return NotFound();
            }

            List<FranchiseAllMoviesDto> dto = _mapper.Map<List<FranchiseAllMoviesDto>>(franchise);

            return dto;
        }

        /// <summary>
        /// Get all characters from a Franchise
        /// </summary>
        [HttpGet("franchise/{id}")]
        public async Task<ActionResult<List<FranchiseDto>>> GetAllFranchise(int id)
        {
            var franchise = await _context.Franchise.Include(f => f.Movies).ThenInclude(f => f.MovieCharacters).ThenInclude(f => f.Character).Where(f => f.ID == id).ToListAsync();

            if (franchise == null)
            {
                return NotFound();
            }

           List<FranchiseDto> dto = _mapper.Map<List<FranchiseDto>>(franchise);

            return dto;
        }

        // PUT: api/Franchises/5
        /// <summary>
        /// Modify a specific Franchise according to its id
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.ID)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        /// <summary>
        /// Create a new Franchise and add it to the database
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
        {
            _context.Franchise.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.ID }, franchise);
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Delete a specific Franchise according to its id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Franchise>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchise.Remove(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.ID == id);
        }
    }
}
