﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13;
using Task13.DTOs.Movie;
using Task13.Models;

namespace Task13.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper; 
        }

        // GET: api/Movies
        /// <summary>
        /// Get all Movies from the database
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Movie>>> GetMovie()
        {
            return await _context.Movie.ToListAsync();
        }


        // GET: api/Movies/5
        /// <summary>
        /// Get a specific Movie according to its id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<SingleMovieDto>> GetMovie(int id)
        {
            var movie = await _context.Movie.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            SingleMovieDto tdo = _mapper.Map<SingleMovieDto>(movie);

            return tdo;
        }

        /// <summary>
        /// Get all characters of a movie including the actor who played that character
        /// </summary>
        [HttpGet("movieschar/{id}")]
        public async Task<ActionResult<List<MovieCharacterDTO>>> GetallMovieChar(int id)
        {
            var movie = await _context.Movie.Include(m => m.MovieCharacters).ThenInclude(m => m.Character).ThenInclude(m => m.MovieCharacters).ThenInclude(m => m.Actor).Where(m => m.ID == id).ToListAsync();

            if (movie == null)
            {
                return NotFound();
            }

            List<MovieCharacterDTO> tdo = _mapper.Map<List<MovieCharacterDTO>>(movie);

            return tdo;
        }

        // PUT: api/Movies/5
        /// <summary>
        /// Modify an existing Movie and updating the database
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            if (id != movie.ID)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Movies
        /// <summary>
        /// Create a new Movie and add it to the database
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(Movie movie)
        {
            _context.Movie.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.ID }, movie);
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Delete a specific Movie according to its id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Movie>> DeleteMovie(int id)
        {
            var movie = await _context.Movie.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movie.Remove(movie);
            await _context.SaveChangesAsync();

            return movie;
        }

        private bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.ID == id);
        }
    }
}
