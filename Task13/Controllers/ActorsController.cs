﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13;
using Task13.DTOs.Actor;
using Task13.Models;

namespace Task13.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper; 

        public ActorsController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Actors
        /// <summary>
        /// Get all actors
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Actor>>> GetActor()
        {
            return await _context.Actor.ToListAsync();
        }

        // GET: api/Actors/5
        /// <summary>
        /// Get a specific actor according to his/her id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorDto>> GetActor(int id)
        {
            var actor = await _context.Actor.FindAsync(id);

            if (actor == null)
            {
                return NotFound();
            }

            ActorDto dto = _mapper.Map<ActorDto>(actor);

            return dto;
        }

        // GET: api/Actors/5
        /// <summary>
        /// Get all movies an actor has played
        /// </summary>
        // Get all movies an actor has played
        [HttpGet("movie/{id}")]
        public async Task<ActionResult<List<ActorDto>>> GetAllActor(int id)
        {
            var actor = await _context.Actor.Include(a => a.MovieCharacters).ThenInclude(a => a.Movie).Where(a => a.ID == id).ToListAsync();


            if (actor == null)
            {
                return NotFound();
            }

            List<ActorDto> dto = _mapper.Map<List<ActorDto>>(actor);

            return dto;
        }

        // GET: api/Actors/5
        /// <summary>
        /// Get all characters an actor has played
        /// </summary>
        [HttpGet("characters/{id}")]
        public async Task<ActionResult<List<ActorAllCharactersDto>>> GetAllMovieActor(int id)
        {
            var actor = await _context.Actor.Include(a => a.MovieCharacters).ThenInclude(a => a.Movie).Include(a => a.MovieCharacters).ThenInclude(a => a.Character).Where(a => a.ID == id).ToListAsync();


            if (actor == null)
            {
                return NotFound();
            }

           List<ActorAllCharactersDto> dto = _mapper.Map<List<ActorAllCharactersDto>>(actor);

            return dto;
        }

        // PUT: api/Actors/5
        /// <summary>
        /// Modify a specific actor object according to his/her id
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.ID)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actors
        /// <summary>
        /// Create a new actor object and add it to the database
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Actor>> PostActor(Actor actor)
        {
            _context.Actor.Add(actor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = actor.ID }, actor);
        }

        // DELETE: api/Actors/5
        /// <summary>
        /// Delte a specific actor according to his/her id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Actor>> DeleteActor(int id)
        {
            var actor = await _context.Actor.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actor.Remove(actor);
            await _context.SaveChangesAsync();

            return actor;
        }

        private bool ActorExists(int id)
        {
            return _context.Actor.Any(e => e.ID == id);
        }
    }
}
