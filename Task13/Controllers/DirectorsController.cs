﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13;
using Task13.Models;

namespace Task13.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DirectorsController : ControllerBase
    {
        private readonly MovieDbContext _context;

        public DirectorsController(MovieDbContext context)
        {
            _context = context;
        }

        // GET: api/Directors
        /// <summary>
        /// Get all Directors from the database
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Director>>> GetDirector()
        {
            return await _context.Director.ToListAsync();
        }

        // GET: api/Directors/5
        /// <summary>
        /// Get a specific Director according to his/her id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<Director>> GetDirector(int id)
        {
            var director = await _context.Director.FindAsync(id);

            if (director == null)
            {
                return NotFound();
            }

            return director;
        }

        // PUT: api/Directors/5
        /// <summary>
        /// Modify a specific Director according to his/her id
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDirector(int id, Director director)
        {
            if (id != director.ID)
            {
                return BadRequest();
            }

            _context.Entry(director).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DirectorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Directors
        /// <summary>
        /// Create a new Director and add him/her to the database
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Director>> PostDirector(Director director)
        {
            _context.Director.Add(director);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDirector", new { id = director.ID }, director);
        }

        // DELETE: api/Directors/5
        /// <summary>
        /// Delete a specific Director according to his/her id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Director>> DeleteDirector(int id)
        {
            var director = await _context.Director.FindAsync(id);
            if (director == null)
            {
                return NotFound();
            }

            _context.Director.Remove(director);
            await _context.SaveChangesAsync();

            return director;
        }

        private bool DirectorExists(int id)
        {
            return _context.Director.Any(e => e.ID == id);
        }
    }
}
