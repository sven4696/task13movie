﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13;
using Task13.DTOs.Character;
using Task13.Models;

namespace Task13.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper; 
        }

        // GET: api/Characters
        /// <summary>
        /// Get all characters from the database
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Character>>> GetCharacter()
        {
            return await _context.Character.ToListAsync();
        }

        // GET: api/Characters/5
        /// <summary>
        /// Get a specific character according to his/her id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDto>> GetCharacter(int id)
        {
            var character = await _context.Character.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            CharacterDto dto = _mapper.Map<CharacterDto>(character);

            return dto;
        }

        /// <summary>
        /// Get all the actors who have played a particular character
        /// </summary>
        [HttpGet("allactors/{id}")]
        public async Task<ActionResult<List<CharacterDto>>> GetAllCharacter(int id)
        {
            var character = await _context.Character.Include(c => c.MovieCharacters).ThenInclude(c => c.Movie).Include(c => c.MovieCharacters).ThenInclude(c => c.Actor).Where(c => c.ID == id).ToListAsync();

            if (character == null)
            {
                return NotFound();
            }

            List<CharacterDto> dto = _mapper.Map<List<CharacterDto>>(character);

            return dto;
        }

        // PUT: api/Characters/5
        /// <summary>
        /// Modify a character object according to his/her id
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.ID)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        /// <summary>
        /// Create a character object and add it to the database
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(Character character)
        {
            _context.Character.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.ID }, character);
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Delete a specific character according to his/her id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Character>> DeleteCharacter(int id)
        {
            var character = await _context.Character.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Character.Remove(character);
            await _context.SaveChangesAsync();

            return character;
        }

        private bool CharacterExists(int id)
        {
            return _context.Character.Any(e => e.ID == id);
        }
    }
}
