﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Actor;
using Task13.DTOs.Character;
using Task13.DTOs.Movie;
using Task13.Models;

namespace Task13.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDto>().ForMember(c => c.MovieCharacters, opt => opt.MapFrom(c => c.MovieCharacters));
            CreateMap<MovieCharacter, MovieCharactersDto>().ForMember(m => m.Actors, opt => opt.MapFrom(m => m.Actor));
            CreateMap<Actor, SingleActor>();
            CreateMap<Movie, SingleMovieDto>();
        }
    }
}
