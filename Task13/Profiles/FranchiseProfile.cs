﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Character;
using Task13.DTOs.Franchise;
using Task13.Models;

namespace Task13.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDto>().ForMember(framo => framo.Movie, opt => opt.MapFrom(a => a.Movies));
            CreateMap<Franchise, FranchiseAllMoviesDto>().ForMember(framo => framo.Movie, opt => opt.MapFrom(a => a.Movies));   
            CreateMap<Movie, FMovieCharactersDto>().ForMember(m => m.MovieCharacters, opt => opt.MapFrom(m => m.MovieCharacters));
            CreateMap<MovieCharacter, FranchiseMoviesDto>().ForMember(m => m.Character, opt => opt.MapFrom(f => f.Character));
            CreateMap<Character, SingleCharacterDto>();
        }
    }
}
