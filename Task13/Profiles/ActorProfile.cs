﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Actor;
using Task13.DTOs.Character;
using Task13.DTOs.Movie;
using Task13.Models;

namespace Task13.Profiles
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            CreateMap<Actor, ActorDto>().ForMember(a => a.MovieCharacters, opt => opt.MapFrom( a=> a.MovieCharacters));
            CreateMap<Actor, ActorAllCharactersDto>().ForMember(a => a.MovieCharacters, opt => opt.MapFrom(a => a.MovieCharacters));
            CreateMap<MovieCharacter, ActorMoviesDto>().ForMember(m => m.Movie, opt => opt.MapFrom(a => a.Movie));
            CreateMap<MovieCharacter, CharactersForActors>().ForMember(m => m.Character, opt => opt.MapFrom(a => a.Character));
            CreateMap<Movie, SingleMovieDto>();
            CreateMap<Character, SingleCharacterDto>();
        }
    }
}
