﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Actor;
using Task13.DTOs.Character;
using Task13.DTOs.Movie;
using Task13.Models;

namespace Task13.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieCharacterDTO>().ForMember(m => m.MovieCharacters, opt => opt.MapFrom(m => m.MovieCharacters));
            CreateMap<MovieCharacter, MovieCharacterWithActor>().ForMember(m => m.Character, opt => opt.MapFrom(m => m.Character));
            CreateMap<MovieCharacter, MovieCharacterWithActor>().ForMember(m => m.Actor, opt => opt.MapFrom(m => m.Actor));
            CreateMap<Actor, SingleActor>();
            CreateMap<Character, SingleCharacterDto>();
        }
    }
}
