﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.Models;

namespace Task13
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Actor> Actor { get; set; }

        public DbSet<Character> Character { get; set; }

        public DbSet<Director> Director { get; set; }

        public DbSet<Franchise> Franchise { get; set; }

        public DbSet<Movie> Movie { get; set; }

        public DbSet<MovieCharacter> MovieCharacter { get; set; }

        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.CharacterID, mc.MovieID });
        }


    }
}
