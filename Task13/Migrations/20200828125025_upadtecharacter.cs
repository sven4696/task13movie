﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Task13.Migrations
{
    public partial class upadtecharacter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MovieCharacterID",
                table: "Character",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MovieCharacterID",
                table: "Character");
        }
    }
}
