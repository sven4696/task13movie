﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Task13.Migrations
{
    public partial class updateTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MovieCharacterID",
                table: "Character");

            migrationBuilder.DropColumn(
                name: "MovieCharacterID",
                table: "Actor");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MovieCharacterID",
                table: "Character",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MovieCharacterID",
                table: "Actor",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
