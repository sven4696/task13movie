﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.DTOs.Actor
{
    public class ActorDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public ICollection<ActorMoviesDto> MovieCharacters { get; set; }

    }
}
