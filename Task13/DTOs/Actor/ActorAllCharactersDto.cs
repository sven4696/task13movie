﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Character;

namespace Task13.DTOs.Actor
{
    public class ActorAllCharactersDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public ICollection<CharactersForActors> MovieCharacters { get; set; }
    }
}
