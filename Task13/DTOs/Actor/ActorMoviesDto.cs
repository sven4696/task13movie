﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Movie;

namespace Task13.DTOs.Actor
{
    public class ActorMoviesDto
    {
        // foreign key
        public int MovieID { get; set; }

        // Navigation property
        public SingleMovieDto Movie { get; set; }
    }
}
