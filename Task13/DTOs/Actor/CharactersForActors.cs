﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Character;
using Task13.DTOs.Movie;

namespace Task13.DTOs.Actor
{
    public class CharactersForActors
    {
        public SingleMovieDto Movie { get; set; }

        public SingleCharacterDto Character { get; set; }

    }
}
