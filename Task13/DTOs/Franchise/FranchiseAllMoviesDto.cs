﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Movie;

namespace Task13.DTOs.Franchise
{
    public class FranchiseAllMoviesDto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<SingleMovieDto> Movie { get; set; }


    }
}
