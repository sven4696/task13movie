﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Movie;
using Task13.Models;

namespace Task13.DTOs.Franchise
{
    public class FranchiseDto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<FMovieCharactersDto> Movie { get; set; }

    }
}
