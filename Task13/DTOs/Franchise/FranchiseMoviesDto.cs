﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Character;

namespace Task13.DTOs.Franchise
{
    public class FranchiseMoviesDto
    {
        public int CharacterID { get; set; }

        // Navigation property
        public SingleCharacterDto Character { get; set; }
    }
}
