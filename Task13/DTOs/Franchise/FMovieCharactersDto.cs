﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.DTOs.Franchise
{
    public class FMovieCharactersDto
    {
        public int ID { get; set; }

        public string MovieTitle { get; set; }

        public ICollection<FranchiseMoviesDto> MovieCharacters { get; set; }

    }
}
