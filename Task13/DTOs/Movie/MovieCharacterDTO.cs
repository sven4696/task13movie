﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Character;

namespace Task13.DTOs.Movie
{
    public class MovieCharacterDTO
    {
        public string MovieTitle { get; set; }
        public string ReleaseYear { get; set; }

        // Navigation property
        public ICollection<MovieCharacterWithActor> MovieCharacters { get; set; }

      
    }
}
