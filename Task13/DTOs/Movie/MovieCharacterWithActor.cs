﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Actor;
using Task13.DTOs.Character;

namespace Task13.DTOs.Movie
{
    public class MovieCharacterWithActor
    {
        public SingleCharacterDto Character { get; set; }

        public SingleActor Actor { get; set; }
    }
}
