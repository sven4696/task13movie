﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.DTOs.Movie
{
    public class SingleMovieDto
    {
        public string MovieTitle { get; set; }

        public string ReleaseYear { get; set; }
        
    }
}
