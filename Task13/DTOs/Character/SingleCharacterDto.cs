﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13.DTOs.Character
{
    public class SingleCharacterDto
    {
        public string FullName { get; set; }

        public string Alias { get; set; }

    }
}
