﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13.DTOs.Actor;
using Task13.DTOs.Movie;

namespace Task13.DTOs.Character
{
    public class MovieCharactersDto
    {
        public SingleMovieDto Movie { get; set; }
        public SingleActor Actors { get; set; }
    }
}
